A simple jsp app, created via

```
mvn archetype:generate -DarchetypeArtifactId=maven-archetype-webapp -DarchetypeGroupId=org.apache.maven.archetypes -DinteractiveMode=false -DgroupId=com.intergral.test -DartifactId=jsp-test -DarchetypeVersion=1.
```

### Profiles

There are 4 profiles (apart from the default) which allow the jsp to be precompiled

```
mvn clean install -P jetty93-jspc
mvn clean install -P jetty93-custom-jspc
mvn clean install -P jetty90-jspc
mvn clean install -P jetty90-custom-jspc
```
